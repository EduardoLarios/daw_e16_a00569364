<?php session_start(); ?>

<?php if(isset($_SESSION["error"])): ?>
    <h3>Error de sesión</h3>
    <?php unset($_SESSION["error"]); ?>
<?php endif; ?>
<?php
	
	require_once "../util.php";

	$ID = $_SESSION["idAlumno"];
	$Nombre = $_POST['NombreUsuario'];
	$ApellidoP = $_POST['APUsuario'];
	$ApellidoM = $_POST['AMUsuario'];
	$CorreoP = $_POST['CorreoPersonal'];
	$Tel = $_POST['Telefono'];
	$Cel = $_POST['Cel'];

	$IDC = getContactID($ID);
	while ($row = $IDC->fetch_assoc()) {
        $idContacto = $row['idContacto'];
        updateContacto($idContacto, $Nombre, $ApellidoP, $ApellidoM, $CorreoP, $Tel, $Cel);
        
    }
	
	header("Location: BuscarUsuario_View.php");
	die();
?>