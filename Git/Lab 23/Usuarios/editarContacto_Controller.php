<?php session_start(); ?>

<?php if(isset($_SESSION["error"])): ?>
    <h3>Error de sesión</h3>
    <?php unset($_SESSION["error"]); ?>
<?php endif; ?>

<?php if(isset($_SESSION["idUsuario"])): ?>
    <?php require_once('../util.php'); ?>
	<?php include_once("../_header.html"); ?>
	<?php include_once("../_menu.html"); ?>
	<?php include_once("EditarContacto.html"); ?>
    <?php include_once("../_footer.html"); ?>

<?php else: ?>
    <?php include("../../index.php"); ?>
<?php endif; ?>



