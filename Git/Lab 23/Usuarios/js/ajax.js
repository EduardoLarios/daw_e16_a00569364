function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } 
  else {
    // Non AJAX browsers
    return(null);
  }
}


function sendRequestBusqueda(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var selectRol = document.getElementById("Rol");
     var idRol = selectRol.value;
     //alert(idCarrera);
     var url ='ssajax.php?idRol=' + idRol;
     //alert(url);
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseBusqueda = document.getElementById('ajaxResponseBusqueda');
                    ajaxResponseBusqueda.innerHTML = request.responseText;
                    ajaxResponseBusqueda.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }  
}

function sendRequestNombre(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var input = document.getElementById("NombreUsuario");
     var NombreUsuario = input.value;
     var url ='ssajax.php?NombreUsuario=' + NombreUsuario;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseAlumnoNombre = document.getElementById('ajaxResponseAlumnoNombre');
                    ajaxResponseAlumnoNombre.innerHTML = request.responseText;
                    ajaxResponseAlumnoNombre.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }
}

function sendRequestNombreProf(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var input = document.getElementById("NombreUsuarioP");
     var NombreUsuarioP = input.value;
     var url ='ssajax.php?NombreUsuarioP=' + NombreUsuarioP;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseProfesorNombre = document.getElementById('ajaxResponseProfesorNombre');
                    ajaxResponseProfesorNombre.innerHTML = request.responseText;
                    ajaxResponseProfesorNombre.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }
}

function sendRequestNombreAdmin(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var input = document.getElementById("NombreUsuarioA");
     var NombreUsuarioA = input.value;
     var url ='ssajax.php?NombreUsuarioA=' + NombreUsuarioA;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseAdminNombre = document.getElementById('ajaxResponseAdminNombre');
                    ajaxResponseAdminNombre.innerHTML = request.responseText;
                    ajaxResponseAdminNombre.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }
}

function sendRequestidAlumno(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var input = document.getElementById("idAlumno");
     var idAlumno = input.value;
     var url ='ssajax.php?idAlumno=' + idAlumno;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseIDAlumno = document.getElementById('ajaxResponseIDAlumno');
                    ajaxResponseIDAlumno.innerHTML = request.responseText;
                    ajaxResponseIDAlumno.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }
}

function sendRequestidProfesor(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var input = document.getElementById("idProfesor");
     var idProfesor = input.value;
     var url ='ssajax.php?idProfesor=' + idProfesor;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseIDProfesor = document.getElementById('ajaxResponseIDProfesor');
                    ajaxResponseIDProfesor.innerHTML = request.responseText;
                    ajaxResponseIDProfesor.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }
}

function sendRequestidAdmin(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var input = document.getElementById("idUsuario");
     var idUsuario = input.value;
     var url ='ssajax.php?idUsuario=' + idUsuario;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseIDAdmin = document.getElementById('ajaxResponseIDAdmin');
                    ajaxResponseIDAdmin.innerHTML = request.responseText;
                    ajaxResponseIDAdmin.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }
}

function sendRequestSexoAlumno(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var Genero = document.getElementById("Genero");
     var Sexo = Genero.value;
     var url ='ssajax.php?Sexo=' + Sexo;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseSexoAlumno = document.getElementById('ajaxResponseSexoAlumno');
                    ajaxResponseSexoAlumno.innerHTML = request.responseText;
                    ajaxResponseSexoAlumno.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }  
}

function sendRequestSexoProf(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var Genero = document.getElementById("Genero");
     var SexoP = Genero.value;
     var url ='ssajax.php?SexoP=' + SexoP;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseSexoProf = document.getElementById('ajaxResponseSexoProf');
                    ajaxResponseSexoProf.innerHTML = request.responseText;
                    ajaxResponseSexoProf.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }  
}

function sendRequestSexoAdmin(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var Genero = document.getElementById("Genero");
     var SexoA = Genero.value;
     var url ='ssajax.php?SexoA=' + SexoA;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponseSexoAdmin = document.getElementById('ajaxResponseSexoAdmin');
                    ajaxResponseSexoAdmin.innerHTML = request.responseText;
                    ajaxResponseSexoAdmin.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }  
}

function sendRequestPlan(){
  console.log("onChange event");
   var request = getRequestObject();
   if(request!=null)
   {
     var idPlan = document.getElementById("Plan").value;
     var url ='ssajax.php?idPlan=' + idPlan;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState == 4)){
                    // Asynchronous response has arrived
                    var ajaxResponsePlan = document.getElementById('ajaxResponsePlan');
                    ajaxResponsePlan.innerHTML = request.responseText;
                    ajaxResponsePlan.style.visibility = "visible";
                    $('select').material_select();
                }     
            };
      
       
     request.send(null);
   }  
}

function sendRequestHistorial($idCal){
   var request=getRequestObject();
   if(request!=null)
   {
     var url='ssajax.php?idCal=' + $idCal;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    // Asynchronous response has arrived
                    var ajaxResponseHistorial = document.getElementById('ajaxResponseHistorial');
                    ajaxResponseHistorial.innerHTML = request.responseText;
                    ajaxResponseHistorial.style.visibility = "visible";
                    $(".dropdown-button").dropdown();
                    $('select').material_select();
                }     
            };

     request.send(null);
   }  
}

function sendRequestEditTrue($editableTrue, $idAlumno){
   var request=getRequestObject();
   if(request!=null)
   {
     var url='ssajax.php?editableTrue=' + $editableTrue + '&id=' + $idAlumno;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    // Asynchronous response has arrived
                    var ajaxResponseEdit = document.getElementById('ajaxResponseEdit');
                    ajaxResponseEdit.innerHTML = request.responseText;
                    ajaxResponseEdit.style.visibility = "visible";
                    $(".dropdown-button").dropdown();
                    $('select').material_select();
                }     
            };

     request.send(null);
   }  
}

function sendRequestEditFalse($editableFalse, $idAlumno){
   var request=getRequestObject();
   if(request!=null)
   {
     var url='ssajax.php?editableFalse=' + $editableFalse + '&id=' + $idAlumno;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    // Asynchronous response has arrived
                    var ajaxResponseEdit = document.getElementById('ajaxResponseEdit');
                    ajaxResponseEdit.innerHTML = request.responseText;
                    ajaxResponseEdit.style.visibility = "visible";
                    $(".dropdown-button").dropdown();
                    $('select').material_select();
                }     
            };

     request.send(null);
   }  
}

function sendRequestEditContactTrue($editableTrueC, $idAlumno){
   var request=getRequestObject();
   if(request!=null)
   {
     var url='ssajax.php?editableTrueC=' + $editableTrueC + '&id=' + $idAlumno;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    // Asynchronous response has arrived
                    var ajaxResponseEdit = document.getElementById('ajaxResponseEdit');
                    ajaxResponseEdit.innerHTML = request.responseText;
                    ajaxResponseEdit.style.visibility = "visible";
                    $(".dropdown-button").dropdown();
                    $('select').material_select();
                }     
            };

     request.send(null);
   }  
}

function sendRequestEditContactFalse($editableFalseC, $idAlumno){
   var request=getRequestObject();
   if(request!=null)
   {
     var url='ssajax.php?editableFalseC=' + $editableFalseC + '&id=' + $idAlumno;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    // Asynchronous response has arrived
                    var ajaxResponseEdit = document.getElementById('ajaxResponseEdit');
                    ajaxResponseEdit.innerHTML = request.responseText;
                    ajaxResponseEdit.style.visibility = "visible";
                    $(".dropdown-button").dropdown();
                    $('select').material_select();
                }     
            };

     request.send(null);
   }  
}