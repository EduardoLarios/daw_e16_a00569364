<?php
require_once('../util.php'); 

if(isset($_GET['idRol'])) {
 $idRol = $_GET['idRol']; ?>  
 <div class="input-field col s4">
   <select id="Busqueda">
    <option value="" disabled selected>Elige el tipo de búsqueda</option>
    <option value="1">Búsqueda por nombre completo</option>
    <option value="2">Búsqueda por ID</option>
    <option value="3">Búsqueda por Sexo</option>
  </select>
</div>

<div class="col s10">
  <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action">
    <a class="white-text" href="#" onclick="redirectUser()">Siguiente</a>
    <i class="material-icons right">trending_flat</i>
  </button>
</div>

<?php
}
?>  



<?php
require_once('../util.php');

if(isset($_GET['NombreUsuario'])) { 

  $Nombre = (string)$_GET['NombreUsuario'];
  $Rol = 1;

  $result = fullNameSearch($Nombre, $Rol) or array();
  ?>

  <div class="section col s10" id="section">

    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?> 
          <tr>
            <td><?= $value['idUsuario'] ?></td>
            <td><strong><?= $value['Nombre'] ?></strong></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarAlumno_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table> 
  </div>

  <?php
}
?>   

<?php
require_once('../util.php');

if(isset($_GET['NombreUsuarioP'])) { 

  $Nombre = (string)$_GET['NombreUsuarioP'];
  $Rol = 2;

  $result = fullNameSearch($Nombre, $Rol) or array();
  ?>

  <div class="section col s10" id="section">

    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?> 
          <tr>
            <td><?= $value['idUsuario'] ?></td>
            <td><strong><?= $value['Nombre'] ?></strong></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarProfesor_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table> 
  </div>

  <?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['NombreUsuarioA'])) { 

  $Nombre = (string)$_GET['NombreUsuarioA'];
  $Rol = 3;

  $result = fullNameSearch($Nombre, $Rol) or array();
  ?>

  <div class="section col s10" id="section">

    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?> 
          <tr>
            <td><?= $value['idUsuario'] ?></td>
            <td><strong><?= $value['Nombre'] ?></strong></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarAdministrador_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table> 
  </div>

  <?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['idAlumno'])) { 

  $ID = (string)$_GET['idAlumno'];

  $result = idSearch($ID) or array();
  ?>

  <div class="section col s10" id="section">

    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Semestre</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?>
          <tr>
            <td><strong><?= $value['idUsuario'] ?></strong></td>
            <td><?= $value['Nombre'] ?></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td><?= $value['Semestre'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarAlumno_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>  
  <?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['idProfesor'])) { 

  $ID = (string)$_GET['idProfesor'];

  $result = idSearchProf($ID) or array();
  ?>

  <div class="section col s10" id="section">

    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?>
          <tr>
            <td><strong><?= $value['idUsuario'] ?></strong></td>
            <td><?= $value['Nombre'] ?></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarProfesor_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>  
  <?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['idUsuario'])) { 

  $ID = (string)$_GET['idUsuario'];

  $result = idSearchAdmin($ID) or array();
  ?>

  <div class="section col s10" id="section">

    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?>
          <tr>
            <td><strong><?= $value['idUsuario'] ?></strong></td>
            <td><?= $value['Nombre'] ?></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarAdministrador_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>  
  <?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['Sexo'])) { 

  $Sexo = (string)$_GET['Sexo'];

  $result = genderSearch($Sexo) or array();
  ?>

  <div class="section col s10" id="section">
    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Sexo</th>
          <th>Semestre</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?>
          <tr>
            <td><?= $value['idUsuario'] ?></td>
            <td><?= $value['Nombre'] ?></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td><strong><?= $value['Sexo'] ?></strong></td>
            <td><?= $value['Semestre'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarAlumno_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>        
  </div>  
  <?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['SexoP'])) { 

  $Sexo = (string)$_GET['SexoP'];

  $result = genderSearchProf($Sexo) or array();
  ?>

  <div class="section col s10" id="section">
    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Sexo</th>
          <th>Cédula Profesional</th>
          <th>Títulos</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?>
          <tr>
            <td><?= $value['idUsuario'] ?></td>
            <td><?= $value['Nombre'] ?></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td><strong><?= $value['Sexo'] ?></strong></td>
            <td><?= $value['NumCedula'] ?></td>
            <td><?= $value['Titulos'] ?></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarProfesor_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>        
  </div>  
<?php
}
?>

<?php
require_once('../util.php');

if(isset($_GET['SexoA'])) { 
  $Sexo = (string)$_GET['SexoA'];
  $result = genderSearchAdmin($Sexo) or array();
?>

  <div class="section col s10" id="section">
    <div class="divider"></div>
    <table class="bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Sexo</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $value): ?>
          <tr>
            <td><?= $value['idUsuario'] ?></td>
            <td><?= $value['Nombre'] ?></td>
            <td><?= $value['ApellidoPaterno'] ?></td>
            <td><?= $value['ApellidoMaterno'] ?></td>
            <td><strong><?= $value['Sexo'] ?></strong></td>
            <td>
              <a class="btn-floating btn-tiny red" href="editarAdministrador_Controller.php?id=<?= $value['idUsuario'] ?>">
                <i class="tiny material-icons">mode_edit</i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>        
  </div>  
<?php
}
?>

<!-- Modal Edit Historial -->

<?php
require_once('../util.php');
if(isset($_GET['idCal'])) { 
  $idCal = (string)$_GET['idCal'];
  $result = getCalificacion($idCal);
?>
  <?php foreach ($result as $value): ?>
  <h5>Editar Historial : <?= $value['Nombre'] ?></h5>
  <?php endforeach; ?>
    <form action="updateHistorial.php" method="POST">

    <div class="row">
      <div class="input-field col s4">
       <?php foreach ($result as $value): ?>
          <input type="hidden" class="validate" value="<?= $value['idCalificacion'] ?>" name="idCal">
          <input type="text" class="validate" disabled value="<?= $value['idCalificacion'] ?>">
          <label class="active" for="idCal">Referencia</label>
      </div>
    </div>

      <div class="row">
        <div class="input-field col s4">
          <input type="text" class="validate" value="<?= $value['CalifFinal'] ?>" name="CalFinal">
          <label class="active" for="CalFinal">Calificación Final</label>
        </div>

        <div class="input-field col s4">
          <input type="text" class="validate" value="<?= $value['FaltasFinal'] ?>" name="FaltasFinal">
          <label class="active" for="FaltasFinal">Faltas Final</label>
        </div>

        <div class="input-field col s4">
            <select name="Extraordinario">
              <option value="<?= $value['Extraordinario'] ?>" disabled selected>Selecciona una opción</option>
              <?php endforeach; ?>
              <option value="1">Extraordinario</option>
              <option value="0">No Extraordinario</option>
            </select>
          <label>Extraordinario</label> 
        </div>

        <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action" onclick="return confirmFunction();">
          Guardar<i class="material-icons right">save</i>
        </button>
        </div>

    </form>  
<?php
}
?>

<!-- Editable Form Alumno True -->

<?php
require_once('../util.php');

if(isset($_GET['editableTrue'])) { 

?>

<?php
  $ID = $_GET['id'];
  $result = idSearch($ID) or array();
  foreach ($result as $value): 
  $_SESSION["idAlumno"] = $value['idAlumno'] 
?> 

  <div class="section col s12" id="section">
    <h5>Editar Alumno</h5>
    <div class="divider"></div>
    <br>
      
      <div class="col s8">
      <a class="white-text" onclick="sendRequestEditFalse(1,<?=$value['idAlumno'] ?>)">
        <button class="btn waves-effect waves-light red accent-4 btn-small">
          Salir de Edición  
          <i class="material-icons right">reply</i>
        </button>
      </a>

      <a class="white-text" href="BuscarUsuario_View.php">
        <button class="btn waves-effect waves-light red accent-4 btn-small" >
          Nueva Búsqueda
          <i class="material-icons right">search</i>
        </button>
      </a>
      </div>
    
    <br>
    <br>
    
    <div class="row">
      <div class="col s8">
        <table class="bordered highlight centered">
              <thead>
                  <tr>
                      <th id="cf" ><strong>Información Alumno</th>
                  </tr>
              </thead>
            </table>
          </div>
        </div>
    
    <form action="updateAlumno_Controller.php" method="post">
      <div class="row">
        <div class="input-field col s8">
          <input name="idUsuario" type="text" class="validate" value="<?= $value['idAlumno'] ?>">
          <label class="active" for="idUsuario">ID</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="NombreUsuario" type="text" class="validate" value="<?= $value['Nombre'] ?>">
          <label class="active" for="NombreUsuario">Nombre</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="APUsuario" type="text" class="validate" value="<?= $value['ApellidoPaterno'] ?>">
          <label class="active" for="APUsuario">Apellido Paterno</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="AMUsuario" type="text" class="validate" value="<?= $value['ApellidoMaterno'] ?>">
          <label class="active" for="AMUsuario">Apellido Materno</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="PassUsuario" type="text" class="validate" value="<?= $value['password'] ?>">
          <label class="active" for="PassUsuario">Contraseña</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="NacimientoUsuario" type="date" class="datepicker" value="<?= $value['FechaNacimiento'] ?>">
          <label class="active" for="NacimientoUsuario">Fecha de Nacimiento</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="CorreoPersonal" type="text" class="validate" value="<?= $value['CorreoPersonal'] ?>">
          <label class="active" for="CorreoPersonal">Correo Personal</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="CorreoInstitucional" type="text" class="validate" value="<?= $value['CorreoInstitucional'] ?>">
          <label class="active" for="CorreoInstitucional">Correo Institucional</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="Telefono" type="text" class="validate" value="<?= $value['Telefono'] ?>">
          <label class="active" for="Telefono">Telefono</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <select name="Sexo">
            <option value="<?= $value['Sexo'] ?>" selected><?= $value['Sexo'] ?></option>
            <option value="Masculino">Masculino</option>
            <option value="Femenino">Femenino</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <select name="Semestre">
            <option value="<?= $value['Semestre'] ?>" selected>Semestre: <?= $value['Semestre'] ?></option>
            <option value="1">1°</option>
            <option value="2">2°</option>
            <option value="3">3°</option>
            <option value="4">4°</option>
            <option value="5">5°</option>
            <option value="6">6°</option>
            <option value="7">7°</option>
            <option value="8">8°</option>
            <option value="9">9°</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <select name="Regular">
            <option value="<?= $value['Regular'] ?>" selected>Tipo de Alumno</option>
            <option value="1">Regular</option>
            <option value="0">Irregular</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <?php $result = getPlanesEstudio(); ?>
          <?php $result2 = getPlanAlumno($ID); ?>
          <select name="Plan">
            <?php foreach ($result2 as $value2): ?>
              <option value="<?=$value2['idPlan'] ?>" disabled selected><?=$value2['Nombre']?> - Versión: <?=$value2['Version']?></option>
            <?php endforeach; ?>
            <?php foreach ($result as $value): ?>
              <option value="<?=$value['idPlan'] ?>"><?=$value['Nombre']?> - Versión: <?=$value['Version']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>

      <div class="row">

        <div class="col s10">
          <a class="white-text" href="#">
            <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action" onclick="return confirmFunction();">
              Editar Contacto Responsable
              <i class="material-icons right">save</i>
            </button>
          </a>
        </div>

        
      </div>

      <div class="divider"></div>
      <br>

    </form>

    <!-- Botones de boleta e historial -->
    
    <?php
    $ID = $_GET['id'];
    $result = idSearch($ID) or array();
    foreach ($result as $value): 
    $_SESSION["idAlumno"] = $value['idAlumno'] 
    ?> 
    <div class="row">
        <div class="col s4">
          <a class="white-text" href="HistorialAcademico_View.php">
            <button class="btn waves-effect waves-light red accent-4 btn-small">
              Historial
              <i class="material-icons right">assignment</i>
            </button>
          </a>

          <a class="white-text" href="Boleta_View.php?id=<?= $value['idAlumno'] ?>">
            <button class="btn waves-effect waves-light red accent-4 btn-small">
              Boleta  
              <i class="material-icons right">import_contacts</i>
            </button>
          </a>
        </div>                      

      </div>
      <?php endforeach; ?>

  </div>
<?php endforeach; ?>

<?php
}
?>





<!--Editable Form Alumno False -->

<?php
require_once('../util.php');

if(isset($_GET['editableFalse'])) { 

?>

<?php
  $ID = $_GET['id'];
  $result = idSearch($ID) or array();
  foreach ($result as $value): 
  $_SESSION["idAlumno"] = $value['idAlumno'] 
?> 

  <div class="section col s12" id="section">
    <h5>Editar Alumno</h5>
    <div class="divider"></div>
    <br>
    
      <div class="col s8">
      <a class="white-text" onclick="sendRequestEditTrue(1,<?=$value['idAlumno'] ?>)">
        <button class="btn waves-effect waves-light red accent-4 btn-small">
          Editar Datos  
          <i class="material-icons right">mode_edit</i>
        </button>
      </a>

      <a class="white-text" href="BuscarUsuario_View.php">
        <button class="btn waves-effect waves-light red accent-4 btn-small" >
          Nueva Búsqueda
          <i class="material-icons right">search</i>
        </button>
      </a>
      </div>
    
    <br>
    <br>
    
    <div class="row">
      <div class="col s8">
        <table class="bordered highlight centered">
              <thead>
                  <tr>
                      <th id="cf" ><strong>Información Alumno</th>
                  </tr>
              </thead>
            </table>
          </div>
        </div>
    
    <form action="updateAlumno_Controller.php" method="post">
      <div class="row">
        <div class="input-field col s8">
          <input name="idUsuario" type="text" class="validate" disabled value="<?= $value['idAlumno'] ?>">
          <label class="active" for="idUsuario">ID</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="NombreUsuario" type="text" class="validate" disabled value="<?= $value['Nombre'] ?>">
          <label class="active" for="NombreUsuario">Nombre</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="APUsuario" type="text" class="validate" disabled value="<?= $value['ApellidoPaterno'] ?>">
          <label class="active" for="APUsuario">Apellido Paterno</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="AMUsuario" type="text" class="validate" disabled value="<?= $value['ApellidoMaterno'] ?>">
          <label class="active" for="AMUsuario">Apellido Materno</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="PassUsuario" type="text" class="validate" disabled value="<?= $value['password'] ?>">
          <label class="active" for="PassUsuario">Contraseña</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="NacimientoUsuario" type="date" class="datepicker" disabled value="<?= $value['FechaNacimiento'] ?>">
          <label class="active" for="NacimientoUsuario">Fecha de Nacimiento</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="CorreoPersonal" type="text" class="validate" disabled value="<?= $value['CorreoPersonal'] ?>">
          <label class="active" for="CorreoPersonal">Correo Personal</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="CorreoInstitucional" type="text" class="validate" disabled value="<?= $value['CorreoInstitucional'] ?>">
          <label class="active" for="CorreoInstitucional">Correo Institucional</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <input name="Telefono" type="text" class="validate" disabled value="<?= $value['Telefono'] ?>">
          <label class="active" for="Telefono">Telefono</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <select disabled name="Sexo">
            <option value="<?= $value['Sexo'] ?>" selected><?= $value['Sexo'] ?></option>
            <option value="Masculino">Masculino</option>
            <option value="Femenino">Femenino</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <select disabled name="Semestre">
            <option value="<?= $value['Semestre'] ?>" selected>Semestre: <?= $value['Semestre'] ?></option>
            <option value="1">1°</option>
            <option value="2">2°</option>
            <option value="3">3°</option>
            <option value="4">4°</option>
            <option value="5">5°</option>
            <option value="6">6°</option>
            <option value="7">7°</option>
            <option value="8">8°</option>
            <option value="9">9°</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <select disabled name="Regular">
            <option value="<?= $value['Regular'] ?>" selected>Tipo de Alumno</option>
            <option value="1">Regular</option>
            <option value="0">Irregular</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s8">
          <?php $result = getPlanesEstudio(); ?>
          <?php $result2 = getPlanAlumno($ID); ?>
          <select disabled name="Plan">
            <?php foreach ($result2 as $value2): ?>
              <option value="<?=$value2['idPlan'] ?>" disabled selected><?=$value2['Nombre']?> - Versión: <?=$value2['Version']?></option>
            <?php endforeach; ?>
            <?php foreach ($result as $value): ?>
              <option value="<?=$value['idPlan'] ?>"><?=$value['Nombre']?> - Versión: <?=$value['Version']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>

      <div class="row">

        <div class="col s10">
          <a class="white-text" href="#">
            <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action" onclick="return confirmFunction();">
              Editar Contacto Responsable
              <i class="material-icons right">save</i>
            </button>
          </a>
        </div>

        
      </div>

      <div class="divider"></div>
      <br>

    </form>

    <!-- Botones de boleta e historial -->
    
    <?php
    $ID = $_GET['id'];
    $result = idSearch($ID) or array();
    foreach ($result as $value): 
    $_SESSION["idAlumno"] = $value['idAlumno'] 
    ?> 
    <div class="row">
        <div class="col s4">
          <a class="white-text" href="HistorialAcademico_View.php">
            <button class="btn waves-effect waves-light red accent-4 btn-small">
              Historial
              <i class="material-icons right">assignment</i>
            </button>
          </a>

          <a class="white-text" href="Boleta_View.php?id=<?= $value['idAlumno'] ?>">
            <button class="btn waves-effect waves-light red accent-4 btn-small">
              Boleta  
              <i class="material-icons right">import_contacts</i>
            </button>
          </a>
        </div>                      

      </div>
      <?php endforeach; ?>

  </div>
<?php endforeach; ?>

<?php
}
?>

<!-- Respuesta Editable Verdadero Contacto -->

<?php
require_once('../util.php');

session_start(); 

if(isset($_GET['editableTrueC'])) { 

$ID = $_SESSION["idAlumno"];
$result = idContactoSearch($ID) or array();
?>

  <?php foreach ($result as $value): ?>
  <div class="section col s10" id="section">
      <?php $ID = $_SESSION["idAlumno"]; ?>
          <?php $resultN = idSearch($ID); ?>
          <?php foreach ($resultN as $valueN): ?>
          <h5>Editar Contacto de: <?= $valueN['Nombre'] ?> <?= $valueN['ApellidoPaterno'] ?> <?= $valueN['ApellidoMaterno'] ?></h5>
          <?php endforeach; ?>
      <div class="divider"></div>
      <br>

      <div class="col s8">
        <a class="white-text" onclick="sendRequestEditContactFalse(1,<?=$value['idAlumno'] ?>)">
          <button class="btn waves-effect waves-light red accent-4 btn-small">
            Editar Datos  
            <i class="material-icons right">mode_edit</i>
          </button>
        </a>

        <a class="white-text" href="BuscarUsuario_View.php">
          <button class="btn waves-effect waves-light red accent-4 btn-small" >
            Nueva Búsqueda
            <i class="material-icons right">search</i>
          </button>
        </a>
      </div>
    
    <br>
    <br>
    
    <div class="row">
      <div class="col s8">
        <table class="bordered highlight centered">
              <thead>
                  <tr>
                      <th id="cf" ><strong>Información Contacto</th>
                  </tr>
              </thead>
            </table>
          </div>
        </div>
      <form action="updateContacto_Controller.php" method="post">

        <div class="row">
          <div class="input-field col s8">
            <input name="NombreUsuario" type="text" class="validate" value="<?= $value['Nombre'] ?>">
            <label class="active" for="NombreUsuario">Nombre</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="APUsuario" type="text" class="validate" value="<?= $value['ApellidoPaterno'] ?>">
            <label class="active" for="APUsuario">Apellido Paterno</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="AMUsuario" type="text" class="validate" value="<?= $value['ApellidoMaterno'] ?>">
            <label class="active" for="AMUsuario">Apellido Materno</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="CorreoPersonal" type="text" class="validate" value="<?= $value['Correo'] ?>">
            <label class="active" for="CorreoPersonal">Correo Personal</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="Telefono" type="text" class="validate" value="<?= $value['Telefono'] ?>">
            <label class="active" for="Telefono">Número de Teléfono</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="Cel" type="text" class="validate" value="<?= $value['Celular'] ?>">
            <label class="active" for="Cel">Número de Celular</label>
          </div>
        </div>

        <div class="row">

          <div class="col s4">
            <a class="white-text" href="#">
            <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action" onclick="return confirmFunction();">
              Guardar Registro
              <i class="material-icons right">save</i>
            </button>
            </a>
          </div>
          
        </div>
      </form>
    </div>
    <?php endforeach; ?>
  </div>
<?php
}
?>   

<!-- Respuesta Editable Falso Contacto -->

<?php
require_once('../util.php');

if(isset($_GET['editableFalseC'])) { 
$ID = $_SESSION["idAlumno"];
$result = idContactoSearch($ID) or array();
?>

  <?php foreach ($result as $value): ?>
  <div class="section col s10" id="section">
      <?php $ID = $_SESSION["idAlumno"]; ?>
          <?php $resultN = idSearch($ID); ?>
          <?php foreach ($resultN as $valueN): ?>
          <h5>Editar Contacto de: <?= $valueN['Nombre'] ?> <?= $valueN['ApellidoPaterno'] ?> <?= $valueN['ApellidoMaterno'] ?></h5>
          <?php endforeach; ?>
      <div class="divider"></div>
      <br>

      <div class="col s8">
        <a class="white-text" onclick="sendRequestEditContactTrue(1,<?=$value['idAlumno'] ?>)">
          <button class="btn waves-effect waves-light red accent-4 btn-small">
            Editar Datos  
            <i class="material-icons right">mode_edit</i>
          </button>
        </a>

        <a class="white-text" href="BuscarUsuario_View.php">
          <button class="btn waves-effect waves-light red accent-4 btn-small" >
            Nueva Búsqueda
            <i class="material-icons right">search</i>
          </button>
        </a>
      </div>
    
    <br>
    <br>
    
    <div class="row">
      <div class="col s8">
        <table class="bordered highlight centered">
              <thead>
                  <tr>
                      <th id="cf" ><strong>Información Contacto</th>
                  </tr>
              </thead>
            </table>
          </div>
        </div>
      <form action="updateContacto_Controller.php" method="post">

        <div class="row">
          <div class="input-field col s8">
            <input name="NombreUsuario" type="text" class="validate" disabled value="<?= $value['Nombre'] ?>">
            <label class="active" for="NombreUsuario">Nombre</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="APUsuario" type="text" class="validate" disabled value="<?= $value['ApellidoPaterno'] ?>">
            <label class="active" for="APUsuario">Apellido Paterno</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="AMUsuario" type="text" class="validate" disabled value="<?= $value['ApellidoMaterno'] ?>">
            <label class="active" for="AMUsuario">Apellido Materno</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="CorreoPersonal" type="text" class="validate" disabled value="<?= $value['Correo'] ?>">
            <label class="active" for="CorreoPersonal">Correo Personal</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="Telefono" type="text" class="validate" disabled value="<?= $value['Telefono'] ?>">
            <label class="active" for="Telefono">Número de Teléfono</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s8">
            <input name="Cel" type="text" class="validate" disabled value="<?= $value['Celular'] ?>">
            <label class="active" for="Cel">Número de Celular</label>
          </div>
        </div>

        <div class="row">

          <div class="col s4">
            <a class="white-text" href="#">
            <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action" onclick="return confirmFunction();">
              Guardar Registro
              <i class="material-icons right">save</i>
            </button>
            </a>
          </div>
          
        </div>
      </form>
    </div>
    <?php endforeach; ?>
  </div>
<?php
}
?>

<!-- Modal Edit Boleta -->

<?php
require_once('../util.php');
if(isset($_GET['idCalBoleta'])) { 
  $idCal = (string)$_GET['idCalBoleta'];
  $result = getCalificacion($idCal);
?>
  <?php foreach ($result as $value): ?>
  <h5>Editar Historial : <?= $value['Nombre'] ?></h5>
  <?php endforeach; ?>
    <form action="updateHistorial.php" method="POST">

    <div class="row">
      <div class="input-field col s4">
       <?php foreach ($result as $value): ?>
          <input type="hidden" class="validate" value="<?= $value['idCalificacion'] ?>" name="idCal">
          <input type="text" class="validate" disabled value="<?= $value['idCalificacion'] ?>">
          <label class="active" for="idCal">Referencia</label>
      </div>
    </div>

      <div class="row">
        <div class="input-field col s4">
          <input type="text" class="validate" value="<?= $value['CalifFinal'] ?>" name="CalFinal">
          <label class="active" for="CalFinal">Calificación Final</label>
        </div>

        <div class="input-field col s4">
          <input type="text" class="validate" value="<?= $value['FaltasFinal'] ?>" name="FaltasFinal">
          <label class="active" for="FaltasFinal">Faltas Final</label>
        </div>

        <div class="input-field col s4">
            <select name="Extraordinario">
              <option value="<?= $value['Extraordinario'] ?>" disabled selected>Selecciona una opción</option>
              <?php endforeach; ?>
              <option value="1">Extraordinario</option>
              <option value="0">No Extraordinario</option>
            </select>
          <label>Extraordinario</label> 
        </div>

        <input type="hidden" value=" <?= $idCal?>" name="idCalificacion">

        <button class="btn waves-effect waves-light red accent-4 btn-small" type="submit" name="action" onclick="return confirmFunction();">
          Guardar<i class="material-icons right">save</i>
        </button>
        </div>

    </form>  
<?php
}
?>
