<?php
	require_once "../util.php";

	$ID = $_POST['idUsuario'];
	$Nombre = $_POST['NombreUsuario'];
	$ApellidoP = $_POST['APUsuario'];
	$ApellidoM = $_POST['AMUsuario'];
	$Contrasena = $_POST['PassUsuario'];
	$FechaNac = $_POST['NacimientoUsuario'];
	$FechaNac = date("Y-m-d", strtotime($FechaNac));
	$CorreoP = $_POST['CorreoPersonal'];
	$CorreoI = $_POST['CorreoInstitucional'];
	$Tel = $_POST['Telefono'];
	$idPlan = $_POST['Plan'];
	$Sexo = $_POST['Sexo'];
	$Semestre = $_POST['Semestre'];
	$Regular = $_POST['Regular'];

	insertAlumno($ID, $Nombre, $ApellidoP, $ApellidoM, $Contrasena, $FechaNac, $CorreoP, $CorreoI, $Tel, $Sexo, $Semestre, $Regular);
	addAlumnoPlan($ID, $idPlan);
	asignarMateriasPlanAlumno($ID, $idPlan);
	header("Location: CrearContacto_View.php");
	die();
?>