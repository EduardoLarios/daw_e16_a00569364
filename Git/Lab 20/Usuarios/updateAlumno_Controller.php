<?php
	require_once "../util.php";

	$ID = $_POST['idUsuario'];
	$Nombre = $_POST['NombreUsuario'];
	$ApellidoP = $_POST['APUsuario'];
	$ApellidoM = $_POST['AMUsuario'];
	$Contrasena = $_POST['PassUsuario'];
	$FechaNac = $_POST['NacimientoUsuario'];
	$FechaNac = date("Y-m-d", strtotime($FechaNac));
	$CorreoP = $_POST['CorreoPersonal'];
	$CorreoI = $_POST['CorreoInstitucional'];
	$Tel = $_POST['Telefono'];
	$Sexo = $_POST['Sexo'];
	$Semestre = $_POST['Semestre'];
	$Regular = $_POST['Regular'];
	$Carrera = $_POST['Carrera'];

	updateAlumno($ID, $Nombre, $ApellidoP, $ApellidoM, $Contrasena, $FechaNac, $CorreoP, $CorreoI, $Tel, $Sexo, $Semestre, $Regular);
	updateAlumnoPlan($ID,$idPlan);
	header("Location: editarContacto_Controller.php");
	die();
?>