<?php
	require_once "../util.php";

	$ID = $_POST['idUsuario'];
	$Nombre = $_POST['NombreUsuario'];
	$ApellidoP = $_POST['APUsuario'];
	$ApellidoM = $_POST['AMUsuario'];
	$Contrasena = $_POST['PassUsuario'];
	$FechaNac = $_POST['NacimientoUsuario'];
	$FechaNac = date("Y-m-d", strtotime($FechaNac));
	$CorreoP = $_POST['CorreoPersonal'];
	$CorreoI = $_POST['CorreoInstitucional'];
	$Tel = $_POST['Telefono'];
	$Sexo = $_POST['Sexo'];
	$NumCedula = $_POST['numCedula'];
	$Empresas = $_POST['empresas'];
	$Titulo = $_POST['titulo'];

	insertProfesor($ID, $Nombre, $ApellidoP, $ApellidoM, $Contrasena, $FechaNac, $CorreoP, $CorreoI, $Tel, $Sexo, $NumCedula, $Empresas, $Titulo);
	header("Location: CrearUsuario_View.php");
	die();
?>