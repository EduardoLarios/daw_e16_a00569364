function squareCube() {
    "use strict";
    var number = prompt("Ingrese el número máximo para calcular su cuadrado y su cubo:", "");
    document.write("<table>");
    document.write("<tr><th>Numero</th><th>Cuadrado</th><th>Cubo</th></tr>");
    for (counter = 1; counter <= number; counter++) {
        document.write("<tr>");

        document.write("<td>");
        document.write(counter);
        document.write("</td>");

        document.write("<td>");
        document.write(Math.pow(counter,2));
        document.write("</td>");

        document.write("<td>");
        document.write(Math.pow(counter,3));
        document.write("</td>");
      
        document.write("</tr>");

    }
    document.write("</table>");
}

function randomAddition() {
    "use strict";
    var n1 = Math.round(Math.random() * 10);
    var n2 = Math.round(Math.random() * 10);
    var correct = n1 + n2;
     var start = new Date();
    var result = prompt("Ingrese el resultado de la suma de: " + n1 + " y " + n2, "");
   
  
    if (result == correct) {
        var end = new Date();
        var dif = end.getTime()-start.getTime();
        alert("El resultado es correcto, tardaste: " + dif/1000 + " segundos");
    }
        else {
            alert("El resultado es incorrecto");
        }
}

function counter() {
    var array = new Array(9)
    var pos = 0;
    var neg = 0;
    var cero = 0;
    
    for(i = 0; i < 10; i++) {
        array[i] = prompt("Dame 10 números entre 10 y -10, uno a la vez:", "");
    }
    
    for(i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            pos++;
        }
        
        else if (array[i] < 0) {
            neg++;
        }
        
        else {
            cero++;
        }
    }
    
    document.write("<table>");
    document.write("<tr><th>Positivos</th><th>Negativos</th><th>Ceros</th></tr>");
    
    document.write("<td>");
    document.write(pos);
    document.write("</td>");
    
    document.write("<td>");
    document.write(neg);
    document.write("</td>");
    
    document.write("<td>");
    document.write(cero);
    document.write("</td>");
    
    document.write("</table>");
      
}

function matrixAverage() {
    
    var numero = 3;
    var matrix = [new Array(numero)];
    for(var i=0; i<numero; i++) {
        matrix[i] = new Array(numero);
        for(var j=0; j<numero; j++) {
            matrix[i][j] = Math.round(Math.random() * 10);
        }  
    }  
    var k=0;
    var promedio = new Array(numero*2);
    var suma=0;
    
    for(var i = 0; i < numero; i++) {
        for(var j = 0; j < numero; j++) { 
            suma += matrix[i][j];
        }
        promedio[k] = suma / numero;
        suma = 0;
        k++;
        
    }
    
    for(var j = 0; j < numero; j++) {
        for(var i = 0; i < numero; i++) {
            suma += matrix[j][i];
            
        }
        promedio[k] = suma / numero;
        suma = 0;
        k++;
    }
    
    for (i = 1; i <= (numero*2); i++) {
        document.write("Promedio: " +i + " es " + promedio[i-1] + "<br>");
    }
    
    
}

function invert() {
    var number = prompt("Dame el número que quieres invertir", "");
    for (var i = number.length; i >= 0; i--) {
        document.write(number.charAt(i));
    }
}

function statsLevel() {
    var hp = 558;
    var hpLevel = 86;
    var armor= 24.3;
    var armorLevel = 3.2;
    var attackSpd = 0.625;
    var spdLevel = 1.035;
    var lvl = prompt(" Calculador de stats en LoL, ame el nivel del campeon, de 1 a 18: ", "1");
    
    document.write("El campeon tiene de stats al nivel " + lvl + " :" + "<br>");
    document.write("HP: " + (hp + (hpLevel*(lvl-1))) + "<br>");
    document.write("Armor: " + (armor + (armorLevel*(lvl-1))) + "<br>");
    document.write("Attack Speed: " + (attackSpd + (spdLevel*(lvl-1))) + "<br>");
       
}
                      