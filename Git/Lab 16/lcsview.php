<?php
  require_once "util.php";

  $result = getDB() or array();

?>

<html>
    <head>
        <meta charset="utf-8">
        <link type="text/css" rel="stylesheet" href="css/materialize.css">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.js"></script>
        <title>Laboratorio 14</title>
    </head>
    
    <body>
        <div class="navbar-fixed">
            <nav>
                <div class="blue-grey darken-1 nav-wrapper" style="padding-left: 5vh">
                    <i class="material-icons" style="display:inline-block">input</i>
                    <a href="index.html" class="brand-logo"><acronym title="Laboratorio 14" style="margin: 30px">Laboratorio 14</acronym></a>
                </div>
            </nav>
        </div>
        
         <div class="container">
             <div class="col s4">
                    <h4>EU LCS Spring Split</h4>
             </div>
             
                              
    <div class="divider">
    </div>
    <div class="section">
    </div>

    <div class="row">
        <div class="col s12 m3">
            <p></p>
        </div>
        
       <table class="bordered highlight">
        <thead>
          <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Team</td>
            <td>Position</td>
            <td>Nationality</td>
            <td>Owner</td>
            <td>Founder</td>
            <td>Coach</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($result as $value): ?>
          <tr>
            <td><?= $value['id'] ?></td>
            <td><?= $value['Name'] ?></td>
            <td><?= $value['Team'] ?></td>
            <td><?= $value['Position'] ?></td>
            <td><?= $value['Nationality'] ?></td>
            <td><?= $value['Owner'] ?></td>
            <td><?= $value['Founder'] ?></td>
            <td><?= $value['Coach'] ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

        <div class="col s12 m2">
            <p></p>
        </div>
        
      </div>

      <div class="section" id="preguntas">
                  <h5>
                    <i class="small mdi-action-question-answer"></i>
                    Preguntas a responder
                  </h5>
                  <ul class="collection">
                            <li class="collection-item">¿Qué es ODBC y para qué es útil?</li>
                            <li class="collection-item">¿Qué es SQL Injection?
                            </li>
                            <li class="collection-item">¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?
                            </li>
                        </ul>
                  <div class="divider"></div>

      <div class="section" id="respuestas">
                  <h5>
                    <i class="material-icons">input</i>
                    Respuestas
                  </h5>
                  <ul class="collection">
                    <li class="collection-item">ODBC es un estándar de acceso a las bases de datos desarrollado por SQL Access Group (SAG) en 1992. El objetivo de ODBC es hacer posible el acceder a cualquier dato desde cualquier aplicación, sin importar qué sistema de gestión de bases de datos (DBMS) almacene los datos.</li>
                    <li class="collection-item">Inyección SQL es un método de infiltración de código intruso que se vale de una vulnerabilidad informática presente en una aplicación en el nivel de validación de las entradas para realizar operaciones sobre una base de datos.
					El origen de la vulnerabilidad radica en el incorrecto chequeo o filtrado de las variables utilizadas en un programa que contiene, o bien genera, código SQL.</li>
                    <li class="collection-item">En el lenguaje PHP, hay diferentes funciones que pueden servir de ayuda para usar con distintos sistemas de gestión de bases de datos.
					Si se usa MySQL, la función a usar es mysql_real_escape_string. No obstante es recomendado usar alternativas que ofrecen consultas preparadas como la clase PDO.</li>
                  </ul>
                  <div class="divider"></div>

        
    </body>
    
    <footer class="page-footer" style="background-color: #546e7a">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="grey-text text-lighten-4">Gracias por su tiempo</h5>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2016 Developed with SublimeText 3 and Materialize CSS/JS Framework
            </div>
          </div>
        </footer>
    
</html>