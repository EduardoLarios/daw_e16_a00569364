-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2016 at 11:45 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eulcs`
--

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `id` int(11) NOT NULL,
  `Team` varchar(50) NOT NULL,
  `Owner` varchar(50) NOT NULL,
  `Founder` varchar(50) NOT NULL,
  `Coach` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `Team`, `Owner`, `Founder`, `Coach`) VALUES
(1, 'Fnatic', 'Finlay Steward', 'Sam Mathews', 'Louis Sevilla'),
(2, 'H2K', 'Oliver Steer', 'Richard Wells', 'Neil Hammad'),
(3, 'Origen', 'Enrique Cedeño Martínez', 'Enrique Cedeño Martínez', 'Nicolai Larsen'),
(4, 'Vitality', 'Fabien Devide', 'Kevin Georges	', 'Kévin Ghanbarzadeh	');

-- --------------------------------------------------------

--
-- Table structure for table `player_roster`
--

CREATE TABLE `player_roster` (
  `id` int(11) NOT NULL,
  `Team` int(11) NOT NULL,
  `Name` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Position` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Nationality` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `player_roster`
--

INSERT INTO `player_roster` (`id`, `Team`, `Name`, `Position`, `Nationality`) VALUES
(1, 1, 'Febiven', 'Mid', 'Netherlands'),
(2, 1, 'Gamsu', 'Top', 'Korea'),
(3, 1, 'Rekkles', 'ADC', 'Sweden'),
(4, 1, 'Spirit', 'Jungle', 'Korea'),
(5, 1, 'Klaj', 'Support', 'Sweden'),
(6, 4, 'KaSing', 'Support', 'United Kingdom'),
(7, 4, 'Shook', 'Jungle', 'Netherlands'),
(8, 4, 'Cabochard', 'Top', 'France'),
(9, 4, 'Nukeduck', 'Mid', 'Norway'),
(10, 4, 'Hjärnan', 'ADC', 'Sweden'),
(11, 3, 'sOAZ', 'Top', 'France'),
(12, 3, 'xPeke', 'Mid', 'Spain'),
(13, 3, 'Zven', 'ADC', 'Denmark'),
(14, 3, 'Amazing', 'Jungle', 'Germany'),
(15, 3, 'Mithy', 'Support', 'Spain'),
(16, 2, 'Odoamne', 'Top', 'Romania'),
(17, 2, 'Jankos', 'Jungle', 'Poland'),
(18, 2, 'Ryu', 'Mid', 'Korea'),
(19, 2, 'Eduardo', 'Jungle', 'Mexico'),
(22, 0, 'Vander', 'Support', 'Poland');

-- --------------------------------------------------------

--
-- Stand-in structure for view `teams`
--
CREATE TABLE `teams` (
`id` int(11)
,`Name` varchar(20)
,`Position` varchar(20)
,`Nationality` varchar(50)
,`Team` varchar(50)
,`Owner` varchar(50)
,`Founder` varchar(50)
,`Coach` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `teams`
--
DROP TABLE IF EXISTS `teams`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teams`  AS  select `p`.`id` AS `id`,`p`.`Name` AS `Name`,`p`.`Position` AS `Position`,`p`.`Nationality` AS `Nationality`,`o`.`Team` AS `Team`,`o`.`Owner` AS `Owner`,`o`.`Founder` AS `Founder`,`o`.`Coach` AS `Coach` from (`player_roster` `p` join `organization` `o`) where (`p`.`Team` = `o`.`id`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `player_roster`
--
ALTER TABLE `player_roster`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `player_roster`
--
ALTER TABLE `player_roster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
