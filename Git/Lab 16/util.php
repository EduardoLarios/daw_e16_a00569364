<?php 

function connectDB(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "eulcs";

	$con = mysqli_connect($servername, $username, $password, $dbname);

	// Check connection
	if(!$con) {
		die("Connection Failed: " . mysqli_connect_error());
	}

	return $con;
	
} 

// La variable $mysql es una conexión establecida anteriormente
function closeDB($mysql){
	mysqli_close($mysql);
}

function getDB(){
	$con = connectDB();
	$sql = "SELECT * FROM `teams`";
	$result = mysqli_query($con, $sql) or die(mysqli_error($con));

	closeDB($con);

	return $result;
}

function getMID(){
	$con = connectDB();
	$sql = "SELECT Name, Nationality, Team FROM teams WHERE Position = 'Mid'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

function getSpaniards(){
	$con = connectDB();
	$sql = "SELECT Name, Position, Team FROM teams WHERE Nationality = 'Spain'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

function getFnatic(){
	$con = connectDB();
	$sql = "SELECT Name, Position, Nationality FROM teams WHERE Team = 'Fnatic'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

function getTeam($team_name){
	$con = connectDB();
	$sql = "SELECT Name, Position, Nationality FROM teams WHERE Team = '%". mysqli_real_escape_string($team_name) ."%'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

	function insertPlayer($Name,$Nationality,$Position,$Team) {
		$con = connectDB();

		$sql = "INSERT INTO player_roster (Name, Nationality, Position, Team) VALUES (\"" . $Name . "\",\"" . $Nationality . "\",\"". $Position . "\",\"" . $Team . "\");";
			
		if (mysqli_query($con, $sql)) {
			echo "New record created succesfully";
			closeDB($con);
			return true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($con);
			closeDB($con);
			return false;
		}

		closeDB($con);

	}

	function deleteByName($player_name) {
		$con = connectDB();

		$sql = "DELETE FROM player_roster WHERE Name = '".$player_name."'";

		if (mysqli_query($con, $sql)) {
			echo "Record deleted succesfully"."<br>";
			closeDB($con);
			return true;
		} 

		else {
			echo "Error: " . $sql . "<br>" . mysqli_error($con);
			closeDB($con);
			return false;
		}

		closeDB($sql);

		}

	function updateByName($id, $Name, $Nationality, $Position, $Team) {
		$con = connectDB();

		$sql = "UPDATE player_roster SET Name = '$Name', Nationality = '$Nationality', Position = '$Position', Team = $Team WHERE id = ".$id;
		
		if (mysqli_query($con, $sql)) {
			echo "Record updated succesfully"."<br>";
			closeDB($con);
			return true;
		} 

		else {
			echo "Error: " . $sql . "<br>" . mysqli_error($con);
			closeDB($con);
			return false;
		}

		closeDB($sql);

		}
?>

