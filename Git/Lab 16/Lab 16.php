<html>
<head>
  <meta charset="utf-8">
  <link type="text/css" rel="stylesheet" href="css/materialize.css">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.js"></script>
  <title>Forma de Registro</title>
</head>

<body>
  <div class="navbar-fixed">
    <nav>
      <div class="blue-grey darken-1 nav-wrapper" style="padding-left: 5vh">
        <i class="material-icons" style="display:inline-block">input</i>
        <a href="index.html" class="brand-logo"><acronym title="Laboratorio 16: PHP" style="margin: 30px">Registro LCS</acronym></a>
      </div>
    </nav>
  </div>

  <div class="container">

    <div class="col s4"><h4>Insert Player</h4></div>

    <div class="row">
      <div class="col s12 m3">
        <p></p>
      </div>

      <div class="row">
        <form class="col s12" action="controllerLab16.php" method="post">
          <div class="row">
            <div class="input-field col s6">
              <input name = "name" type="text" class="validate">
              <label for = "name">Name</label>
            </div>
            <div class="input-field col s6">
              <input name = "nationality" type="text" class="validate">
              <label for = "nationality">Nationality</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <input name = "position" type="text" class="validate">
              <label for = "position">Position</label>
            </div>
            <div class="input-field col s6">
              <input name = "team" type="text" class="validate">
              <label for = "team">Team</label>
            </div> 
          </div>
          <button class="btn blue-grey darken-1" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </form>
      </div>

      <div class="col s12 m2">
        <p></p>
      </div>

    </div>

    <div class="divider"></div>
    <div class="section"></div>

    <div class="col s4"><h4>Delete Player</h4></div>

    <div class="row">
      <form class="col s12" action="controllerDelete.php" method="post">
        <div class="row">
          <div class="input-field col s6">
            <input name = "name_player" type="text" class="validate">
            <label for = "name_player">Name</label>
          </div>
        </div>
        <button class="btn blue-grey darken-1" type="submit" name="action">Submit
          <i class="material-icons right">send</i>
        </button>
      </form>
    </div>

    <div class="divider"></div>
    <div class="section"></div>

    <div class="col s4">
      <h4>Update Player</h4>
    </div>

    <div class="row">
    <form class="col s12" action="updateController.php" method="post">
        <div class="row">
          <div class="input-field col s4">
            <input name = "id" type="text" class="validate">
            <label for = "id">ID</label>
          </div>

          <div class="input-field col s4">
            <input name = "name" type="text" class="validate">
            <label for = "name">Name</label>
          </div>

          <div class="input-field col s4">
            <input name = "nationality" type="text" class="validate">
            <label for = "nationality">Nationality</label>
          </div>

        </div>

        <div class="row">

          <div class="input-field col s6">
            <input name = "position" type="text" class="validate">
            <label for = "position">Position</label>
          </div>

          <div class="input-field col s6">
            <input name = "team" type="text" class="validate">
            <label for = "team">Team</label>
          </div> 

        </div>

        <button class="btn blue-grey darken-1" type="submit" name="action">Submit
          <i class="material-icons right">send</i>
        </button>
      </form>
    </div>

    <div class="section" id="preguntas">
      <h5>
        <i class="small mdi-action-question-answer"></i>
        Preguntas a responder
      </h5>
      <ul class="collection">
        <li class="collection-item">¿Por qué es una buena práctica separar el modelo del controlador?.
        </li>
      </ul>
      <div class="divider"></div>

      <div class="section" id="respuestas">
        <h5>
          <i class="material-icons">input</i>
          Respuestas
        </h5>
        <ul class="collection">
          <li class="collection-item">Ya que así nos aseguramos de la funcionalidad del sistema, ya que el controlador captura el input que manda el usuario accede al modelo, el cual genera una vista que regresar al usuario, asegurándose de correcta separación de interfaz y lógica.</li>
        </ul>
        <div class="divider"></div>


      </body>

      <footer class="page-footer" style="background-color: #546e7a">
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="grey-text text-lighten-4">Gracias por su tiempo</h5>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="container">
            © 2016 Developed with SublimeText 3 and Materialize CSS/JS Framework
          </div>
        </div>
      </footer>

      </html>