<?php
// Array with names
$a[] = "Ana";
$a[] = "Brenda";
$a[] = "Carlos";
$a[] = "Diana";
$a[] = "Eduardo";
$a[] = "Fernanda";
$a[] = "Gilberto";
$a[] = "Hugo";
$a[] = "Ileana";
$a[] = "Juan";
$a[] = "Katarina";
$a[] = "Linda";
$a[] = "Norma";
$a[] = "Oscar";
$a[] = "Pedro";
$a[] = "Amanda";
$a[] = "Rocio";
$a[] = "Carla";
$a[] = "Daniela";
$a[] = "Hector";
$a[] = "Francisco";
$a[] = "Sonia";
$a[] = "Dario";
$a[] = "Miguel";
$a[] = "Alex";
$a[] = "Jorge";
$a[] = "Guillermo";
$a[] = "Helen";
$a[] = "Estela";
$a[] = "Marlene";

// get the q parameter from URL
$q = $_REQUEST["q"];

$hint = "";

// lookup all hints from array if $q is different from "" 
if ($q !== "") {
    $q = strtolower($q);
    $len=strlen($q);
    foreach($a as $name) {
        if (stristr($q, substr($name, 0, $len))) {
            if ($hint === "") {
                $hint = $name;
            } else {
                $hint .= ", $name";
            }
        }
    }
}

// Output "no suggestion" if no hint was found or output correct values 
echo $hint === "" ? "Sin sugerencias" : $hint;
?>