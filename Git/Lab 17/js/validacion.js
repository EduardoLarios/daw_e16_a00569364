function validacion() {
	var password = document.getElementById("pass");
	var validacion = document.getElementById("valid");

	if (validacion.value == "") {
		alert("Ingrese el usuario.");
		validacion.focus();
		return false;
	}
	if (password.value == "") {
		alert("Ingrese la contraseña.");
		password.focus();
		return false;
	}

	if (password.value == "alumno" && validacion.value == "alumno") {
		window.location.assign("Alumno/InicioAlumnos1.html");
	}
	if (password.value == "profesor" && validacion.value == "profesor") {
		window.location.assign("Maestro/InicioMaestro.html");

	}
	if (password.value == "administrador" && validacion.value == "administrador") {
		window.location.assign("Administrador/InicioAdministrador.html");
	}

	if (password.value == "alumno" && validacion.value != "alumno") {
		window.alert("Contraseña inválida");
	}
	if (password.value == "profesor" && validacion.value != "profesor") {
		window.alert("Contraseña inválida");
	}
	if (password.value == "administrador" && validacion.value != "administrador") {
		window.alert("Contraseña inválida");
	}


	return true;
}