<html>
<head>
  <meta charset="utf-8">
  <link type="text/css" rel="stylesheet" href="css/materialize.css">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.js"></script>
  <script type="text/javascript" src="ajax.js"></script>

  <script>
    function showHint(str) {
        if (str.length == 0) { 
            document.getElementById("txtHint").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            };
            xmlhttp.open("GET", "gethint.php?q=" + str, true);
            xmlhttp.send();
        }
    }
  </script>

  <title>AJAX</title>
</head>

<body>
  <div class="navbar-fixed">
    <nav>
      <div class="blue-grey darken-1 nav-wrapper" style="padding-left: 5vh">
        <i class="material-icons" style="display:inline-block">input</i>
        <a href="index.html" class="brand-logo"><acronym title="Laboratorio 17: AJAX" style="margin: 30px">Input AJAX</acronym></a>
      </div>
    </nav>
  </div>

  <div class="container">

    <div class="row">
      <div class="col s4">
        <div><h4>Laboratorio 17</h4></div>
      </div>
    </div>

      <div class="row">
        <form> 
        Nombre: <input type="text" onkeyup="showHint(this.value)">
        </form>
        <p>Sugerencias: <span id="txtHint"></span></p>
      </div>
      
      <div class="row">
        <div class="col s12 m6">
          <div id="demo"><h5>Primer prueba de AJAX</h5></div>
          <br>

          <button class="btn blue-grey darken-1" style="align-left" type="button" onclick="loadDoc()">Presiona para cambiar contenido</button>

          <script>
            function loadDoc() {
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                  document.getElementById("demo").innerHTML = xhttp.responseText;
                }
              };
              xhttp.open("GET", "ajax_info.txt", true);
              xhttp.send();
            }
          </script>
        </div>
      </div>
      
    <div class="section" id="preguntas">
      <h5>
        <i class="small mdi-action-question-answer"></i>
        Preguntas a responder
      </h5>
      <ul class="collection">
        <li class="collection-item">¿Por qué es una buena práctica separar el modelo del controlador?.
        </li>
        <li class="collection-item">¿Qué importancia tiene AJAX en el desarrollo de RIA's (Rich
                      Internet Applications?</li>
        <li class="collection-item">¿Qué implicaciones de seguridad tiene AJAX? ¿Dónde se deben
                      hacer las validaciones de seguridad, del lado del cliente o
                      del lado del servidor?</li>
        <li class="collection-item">¿Qué es JSON?</li>
      </ul>
    </div>

    <div class="section" id="respuestas">
      <h5>
        <i class="material-icons">input</i>
        Respuestas
      </h5>
      <ul class="collection">
        <li class="collection-item">Ya que así nos aseguramos de la funcionalidad del sistema, ya que el controlador captura el input que manda el usuario accede al modelo, el cual genera una vista que regresar al usuario, asegurándose de correcta separación de interfaz y lógica.</li>
        <li class="collection-item">Ya qye permite tener aplicaciones que se actualizan en tiempo real, por lo que el usuario puede ver la retroalimentación a su entrada inmediatamente después. También permite el uso de interfaces que se ajustan al usuario.</li>
        <li class="collection-item">AJAX utiliza javascript por lo que es susceptible a los mismo tipos de ataque utiliza java, es recomendable hacer doble verificación para evitar problemas de seguridad.</li>
        <li class="collection-item">Es un formato de notación para objetos de Javascrip y define la sintaxis para guardar e intercambiar datos, es una alternativa más fácil de utilizar a XML.</li>
      </ul>
      <div class="divider"></div>
    </div>

  </div>
</body>

<footer class="page-footer" style="background-color: #546e7a">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="grey-text text-lighten-4">Gracias por su tiempo</h5>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      © 2016 Developed with SublimeText 3 and Materialize CSS/JS Framework
    </div>
  </div>
</footer>

</html>