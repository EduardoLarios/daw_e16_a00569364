function changeStyle() {
    document.getElementById("miP").style.fontStyle = "italic";
}

function ayudaN() {
    document.getElementById("ayuda").innerHTML("Escribe tu nombre en el cuadro de arriba");
}

function ayudaA() {
    document.getElementById("ayuda").innerHTML("Escribe tu apellido en el cuadro de arriba");
}

function intervalo() {
    setInterval(function(){ alert("Pasaron 5 segundos"); }, 5000);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}