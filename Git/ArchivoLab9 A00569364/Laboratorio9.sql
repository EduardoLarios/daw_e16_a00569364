-- Laboratorio 9 --



-- Primera Parte --

-- Consulta 1 --
-- La suma de las cantidades e importe total de todas las entregas 
-- realizadas durante el 97.

set dateformat dmy

SELECT SUM(Entregan.Cantidad) AS 'Total Cantidad', SUM( (Materiales.Costo + (Materiales.Costo * Materiales.PorcentajeImpuesto* .01 ) ) * Entregan.Cantidad) AS 'Importe Total'
FROM Materiales, Entregan
WHERE Entregan.Fecha >= '01/01/97'
AND Entregan.Fecha <= '31/12/97'
AND Materiales.Clave = Entregan.Clave





-- Consulta 2 --
-- Para cada proveedor,  obtener la razón social del proveedor,
-- número de entregas e importe total de las entregas realizadas.

SELECT Proveedores.RazonSocial, COUNT(Entregan.RFC) AS 'Total Entregas', SUM( (Materiales.Costo + (Materiales.Costo * Materiales.PorcentajeImpuesto* .01 ) ) * Entregan.Cantidad) AS 'Importe Total'
FROM Proveedores, Entregan, Materiales
WHERE Entregan.RFC = Proveedores.RFC
AND Materiales.Clave = Entregan.Clave
GROUP BY Proveedores.RazonSocial




-- Consulta 3 --
 -- Por cada material obtener la clave y descripción del material,
 -- la cantidad total entregada, la mínima cantidad entregada,
 -- la máxima cantidad entregada, el importe total de las entregas 
 -- de aquellos materiales en los que la cantidad promedio 
 -- entregada sea mayor a 400.

SELECT Materiales.Clave,
	   Materiales.Descripcion,
	   SUM(Entregan.Cantidad) AS 'Cantidad Total',
	   MIN(Entregan.Cantidad) AS 'Cantidad Minima',
	   MAX(Entregan.Cantidad) AS 'Cantidad Maxima',
	   SUM( (Materiales.Costo + (Materiales.Costo * Materiales.PorcentajeImpuesto* .01 ) ) * Entregan.Cantidad) AS 'Importe Total'
FROM Materiales, Entregan
WHERE Materiales.Clave = Entregan.Clave
GROUP BY Materiales.Clave, Materiales.Descripcion
HAVING AVG(Entregan.Cantidad) >= 400






-- Consulta 4 --
-- Para cada proveedor, indicar su razón social y mostrar la 
-- cantidad promedio de cada material entregado, detallando 
-- la clave y descripción del material, excluyendo aquellos 
-- proveedores para los que la cantidad promedio sea menor a 500.

SELECT Proveedores.RazonSocial,
	   Materiales.Clave,
	   Materiales.Descripcion,
	   AVG(Entregan.Cantidad) AS 'Promedio de Materiales'
FROM Materiales, Proveedores, Entregan
WHERE Materiales.Clave = Entregan.Clave
AND Proveedores.RFC = Entregan.RFC
GROUP BY Proveedores.RazonSocial, Materiales.Clave, Materiales.Descripcion
HAVING AVG(Entregan.Cantidad) >= 500




-- Consulta 5 --
-- Mostrar en una solo consulta los mismos datos que en la 
-- consulta anterior pero para dos grupos de proveedores: 
-- aquellos para los que la cantidad promedio entregada es 
-- menor a 370 y aquellos para los que la cantidad promedio 
-- entregada sea mayor a 450.

SELECT Proveedores.RazonSocial,
	   Materiales.Clave,
	   Materiales.Descripcion,
	   AVG(Entregan.Cantidad) AS 'Promedio de Materiales'
FROM Materiales, Proveedores, Entregan
WHERE Materiales.Clave = Entregan.Clave
AND Proveedores.RFC = Entregan.RFC
GROUP BY Proveedores.RazonSocial, Materiales.Clave, Materiales.Descripcion
HAVING AVG(Entregan.Cantidad) >= 450 OR AVG(Entregan.Cantidad) <= 370





-- Segunda Parte --

-- Consulta 1 --
-- Clave y descripción de los materiales que 
-- nunca han sido entregados.


SELECT Materiales.Clave, Materiales.Descripcion
FROM Materiales, Entregan
WHERE Entregan.Cantidad = 0
AND Entregan.Clave = Materiales.Clave



-- Consulta 2 --
-- Razón social de los proveedores que han realizado 
-- entregas tanto al proyecto 'Vamos México' como al 
-- proyecto 'Querétaro Limpio'.



SELECT Proveedores.RazonSocial
FROM Proveedores, Proyectos, Entregan
WHERE Proyectos.Numero = Entregan.Numero
AND Entregan.RFC = Proveedores.RFC
AND Proyectos.Denominacion = 'Vamos Mexico'
AND EXISTS (SELECT Proveedores.RazonSocial
	 FROM Proveedores, Proyectos, Entregan
	 WHERE Proyectos.Numero = Entregan.Numero
	 AND Entregan.RFC = Proveedores.RFC
	 AND Proyectos.Denominacion = 'Queretaro limpio'
	)
GROUP BY Proveedores.RazonSocial


-- Consulta 3 --
-- Descripción de los materiales que nunca han sido entregados 
-- al proyecto 'CIT Yucatán'.

SELECT Materiales.Descripcion
FROM Materiales, Proyectos, Entregan
WHERE Materiales.Clave = Entregan.Clave
AND Proyectos.Numero = Entregan.Numero
AND Proyectos.Denominacion != 'CIT Yucatan'
GROUP BY Materiales.descripción



-- Consulta 4 --
-- Razón social y promedio de cantidad entregada de los proveedores
-- cuyo promedio de cantidad entregada es mayor al promedio de 
-- la cantidad entregada por el proveedor con el RFC 'VAGO780901'.

SELECT Proveedores.RazonSocial,
	   AVG(Entregan.Cantidad) AS 'Promedio'
FROM Proveedores, Entregan
WHERE Proveedores.RFC = Entregan.RFC
GROUP BY Proveedores.RazonSocial
HAVING AVG(Entregan.Cantidad) > (SELECT AVG(Entregan.Cantidad)
								 FROM Proveedores, Entregan
								 WHERE Proveedores.RFC = Entregan.RFC
								 AND Proveedores.RFC = 'VAGO780901'
								)




-- Consulta 5 --
-- RFC, razón social de los proveedores que participaron en el 
-- proyecto 'Infonavit Durango' y cuyas cantidades totales 
-- entregadas en el 2000 fueron mayores a las cantidades totales 
-- entregadas en el 2001.

set dateformat dmy

SELECT Proveedores.RFC,
	   Proveedores.RazonSocial
FROM Proveedores, Entregan, Proyectos
WHERE Entregan.Fecha >= '01/01/00'
AND Entregan.Fecha <= '31/12/00'
AND Entregan.Numero = Proyectos.Numero
AND Proveedores.RFC = Entregan.RFC
AND Proyectos.Denominacion = 'Infonavit Durango'
GROUP BY Proveedores.RFC, Proveedores.RazonSocial
HAVING SUM(Entregan.Cantidad) > (SELECT SUM(Entregan.Cantidad)
								 FROM Proveedores, Entregan, Proyectos
								 WHERE Entregan.Fecha >= '01/01/01'
								 AND Entregan.Fecha <= '31/12/01'
								 AND Entregan.Numero = Proyectos.Numero
								 AND Proveedores.RFC = Entregan.RFC
								 AND Proyectos.Denominacion = 'Infonavit Durango'
								)



