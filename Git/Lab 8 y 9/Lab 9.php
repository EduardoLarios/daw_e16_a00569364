<html>
    <head>
        <meta charset="utf-8">
        <link type="text/css" rel="stylesheet" href="css/materialize.css">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.js"></script>
        <title>Forma de Registro</title>
    </head>
    
    <body>
        <div class="navbar-fixed">
            <nav>
                <div class="blue-grey darken-1 nav-wrapper" style="padding-left: 5vh">
                    <i class="material-icons" style="display:inline-block">input</i>
                    <a href="index.html" class="brand-logo"><acronym title="Laboratorio 9: PHP" style="margin: 30px">Forma de Registro</acronym></a>
                </div>
            </nav>
        </div>
        
         <div class="container">
             <div class="col s4">
                    <h4>Información</h4>
             </div>
             
                              
    <div class="divider">
    </div>
    <div class="section">
    </div>

    <div class="row">
        <div class="col s12 m3">
            <p></p>
        </div>
        
        <div class="row">
            <form class="col s12" action="Logica.php" method="post">
              <div class="row">
                <div class="input-field col s6">
                  <input name = "name" type="text" class="validate">
                  <label for = "first_name">Nombre</label>
                </div>
                <div class="input-field col s6">
                  <input name = "last_name" type="text" class="validate">
                  <label for = "last_name">Apellido</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input name = "password" type="password" class="validate">
                  <label for = "password">Contraseña</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input name = "email" type="email" class="validate">
                  <label for = "email">Email</label>
                </div>
              </div>
            <button class="btn blue-grey darken-1" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
            </form>
        </div>

        <div class="col s12 m2">
            <p></p>
        </div>
        
      </div>

      <div class="section" id="preguntas">
                  <h5>
                    <i class="small mdi-action-question-answer"></i>
                    Preguntas a responder
                  </h5>
                  <ul class="collection">
                    <li class="collection-item">¿Por qué es una buena práctica separar el controlador de la
                      vista?.</li>
                    <li class="collection-item">Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos
                      están predefinidos en php y cuál es su función?</li>
                    <li class="collection-item">Explora las funciones de php, y describe 2 que no hayas
                      visto en otro lenguaje y que llamen tu atención.</li>
                  </ul>
                  <div class="divider"></div>

      <div class="section" id="respuestas">
                  <h5>
                    <i class="material-icons">input</i>
                    Respuestas
                  </h5>
                  <ul class="collection">
                    <li class="collection-item">Separar las funciones de la aplicación en modelos, vistas y controladores hace que la aplicación sea muy ligera. Estas características nuevas se añaden fácilmente y las antiguas toman automáticamente una forma nueva.</li>
                    <li class="collection-item">ECHO: Permite hacer output de todos los parámetros.<br> FGETS: Obtiene una línea de un file pointer.<br> CHMOD: Cambia el modo del archivo especificado al modo especificado.</li>
                    <li class="collection-item">phpinfo: Es una función que muestra las extensiones añadidas a PHP y es muy útil para revisar su funcionalidad.<br>array_multisort: Permite ordenar en arreglos multidimensionales.</li>
                  </ul>
                  <div class="divider"></div>

        
    </body>
    
    <footer class="page-footer" style="background-color: #546e7a">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="grey-text text-lighten-4">Gracias por su tiempo</h5>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2016 Developed with SublimeText 3 and Materialize CSS/JS Framework
            </div>
          </div>
        </footer>
    
</html>