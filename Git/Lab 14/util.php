<?php 

function connectDB(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "eulcs";

	$con = mysqli_connect($servername, $username, $password, $dbname);

	// Check connection
	if(!$con) {
		die("Connection Failed: " . mysqli_connect_error());
	}

	return $con;
	
} 

// La variable $mysql es una conexión establecida anteriormente
function closeDB($mysql){
	mysqli_close($mysql);
}

function getDB(){
	$con = connectDB();
	$sql = "SELECT * FROM `teams`";
	$result = mysqli_query($con, $sql) or die(mysqli_error($con));

	closeDB($con);

	return $result;
}

function getMID(){
	$con = connectDB();
	$sql = "SELECT Name, Nationality, Team FROM teams WHERE Position = 'Mid'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

function getSpaniards(){
	$con = connectDB();
	$sql = "SELECT Name, Position, Team FROM teams WHERE Nationality = 'Spain'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

function getFnatic(){
	$con = connectDB();
	$sql = "SELECT Name, Position, Nationality FROM teams WHERE Team = 'Fnatic'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

function getTeam($team_name){
	$con = connectDB();
	$sql = "SELECT Name, Position, Nationality FROM teams WHERE Team = '%". mysqli_real_escape_string($team_name) ."%'";
	$result = mysqli_query($con, $sql);

	closeDB($con);

	return $result;
}

?>

